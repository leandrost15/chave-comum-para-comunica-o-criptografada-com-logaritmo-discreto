#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
#
## Author
# moinho
#
#  IP.py
# testado em (python3.x) somente

def remetente(senha,assinatura):
    return senha * assinatura

def dest(chave_publica,assinatura):
    return chave_publica * assinatura

def del_assinatura(chave_publica,assinatura):
    return chave_publica / assinatura

assinatura_remetente = 53
assinatura_dest = 80

chave_publica = remetente(251410251410, assinatura_remetente)
chave_publica = dest(chave_publica, assinatura_dest)
chave_publica = del_assinatura(chave_publica,assinatura_remetente)
chave_publica = del_assinatura(chave_publica, assinatura_dest)
senha_pura = chave_publica

print(senha_pura)